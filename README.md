# gibbon-tractography-protocols



## This repository

This repository contains files associated with the scientific publication:

Bryant KL, Manger PR, Bertelsen MF, Khrapitchev AA, Sallet J, Benn RA, Mars RB (submitted) A comprehenive atlas of white matter tracts in a lesser ape, the lar gibbon

Please note the LICENSE file

## Tractography protocols

This directory `tractography_protocols` contains the protcols or recipes used to generate the white matter tracts using probabilistic tractography. The subfolders for each contain at least a seed, target (i.e., waypoint), and exclusion (i.e., avoid) mask. Sometimes there is also a stop mask or an 'invert' file that indicates the protocol should also be run with the seed and target switched and the results averaged. These recipes are consistent with the [XTRACT tool](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/XTRACT) (Warrington et al., 2020, NeuroImage), part of [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/).

## tracts_thresh_final and tracts_unthresh_final

The `tracts_thresh` and `tracts_unthresh` contain the tractography results using the above protocols.

## Data

The diffusion MRI data themselves will be made available from the [WIN Digital Brain Bank](https://open.win.ox.ac.uk/DigitalBrainBank/#/) (Tender et al., 2022, eLife) upon acceptance of the paper in a peer reviewed journal.